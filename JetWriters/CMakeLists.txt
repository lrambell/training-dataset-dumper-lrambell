#
# Cmake for JetWriters
#

# Set the name of the package:
atlas_subdir( JetWriters )

# External(s) used by the package:
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)

# We don't want any warnings in compilation
add_compile_options(-Werror)

# We have to define the libraries first because we might not use
# some. This lets us add them condionally.
set(LINK_LIBRARIES
  xAODTracking
  xAODJet
  xAODPFlow
  xAODEgamma
  HDF5Utils
  FlavorTagDiscriminants
  )

# common requirements
atlas_add_library(JetWriters
  src/addCustomConsumer.cxx
  src/JetTrackWriter.cxx
  src/getJetLinkWriter.cxx
  PUBLIC_HEADERS JetWriters
  LINK_LIBRARIES ${LINK_LIBRARIES}
  )


