mkdocs >= 1.4.0
mkdocs-material >= 8.5.0
mkdocs-markdownextradata-plugin >= 0.2.5
mkdocs-mermaid2-plugin >= 0.5.2
mkdocs-git-revision-date-localized-plugin
