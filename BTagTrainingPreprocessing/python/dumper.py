
"""
Dumper interface

"""

import argparse
import os
from pathlib import Path

from GaudiKernel.Configurable import DEBUG

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

class DumperHelpFormatter(
    argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
):
    ...


def base_parser(description, add_help=True):
    parser = argparse.ArgumentParser(description=description, formatter_class=DumperHelpFormatter, add_help=add_help)
    parser.add_argument(
        "input_files", nargs="+",
        help="comma or space separated list of input filenames")
    parser.add_argument(
        "-o", "--output", default=Path("output.h5"), type=Path,
        help="output filename")
    parser.add_argument(
        "-j", "--jet_collection", required=True, type=str,
        help="jet collection")
    parser.add_argument(
        "-c", "--config-file", required=True, type=Path,
        help="job configuration file")    
    parser.add_argument(
        "-m", "--max-events",
        type=int, nargs="?",
        help="number of events to process")
    parser.add_argument(
        "-i", "--event-print-interval",
        type=int, default=500,
        help="set output frequency")
    parser.add_argument(
        '-p', '--force-full-precision',
        action='store_true',
        help='force all half-precision outputs to full precision')
    parser.add_argument(
        '--FPEAuditor',
        action='store_true',
        help='error if floating point exception occurs (requires Athena)')
    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="set output level to DEBUG")
    parser.add_argument(
        "-g", "--debugger",
        action="store_true",
        help="attach debugger at execute step")
    return parser


def update_cfgFlags(cfgFlags, args):

    # parse input files
    if len(args.input_files) > 1:
        if any("," in f for f in args.input_files):
            raise ValueError(
                "you provided a mix of spaces and commas"
                " in your input file list, this is probably an error"
            )
        cfgFlags.Input.Files = [str(x) for x in args.input_files]
    else:
        cfgFlags.Input.Files = str(args.input_files[0]).rstrip(",").split(",")

    # set number of events to process
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events

    # set output level
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG

    # attach debugger
    if args.debugger:
        prepGdb()
        cfgFlags.Exec.DebugStage = "init"

    # error on floating point exceptions
    if args.FPEAuditor:
        cfgFlags.Exec.FPE = -1


def prepGdb():
    """
    Running gdb is sometimes more difficult than it should be, this
    runs some workarounds for the debugger in images.
    """
    # The environment variable I'm checking here just happens to be
    # set to a directory you would not have access to outside images.
    if os.environ.get('LCG_RELEASE_BASE') == '/opt/lcg':
        del os.environ['PYTHONHOME']


def getMainConfig(flags, args):
    ca = MainServicesCfg(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))
    # Avoid stack traces to the exception handler. These traces
    # aren't very useful since they just point to the handler, not
    # the original bug.
    ca.addService(CompFactory.ExceptionSvc(Catch="NONE"))
    return ca


def getDumperConfig(args):
    ca = ComponentAccumulator()

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    btagAlg = CompFactory.SingleBTagAlg(f'{args.config_file.stem}DatasetDumper')
    btagAlg.output = output
    #btagAlg.jet_collection = str(args.jet_collection)
    btagAlg.configFileName = str(args.config_file)
    btagAlg.forceFullPrecision = args.force_full_precision
    ca.addEventAlgo(btagAlg)

    return ca
