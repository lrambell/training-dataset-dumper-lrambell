#include "TruthTools.hh"

#include "AthContainers/ConstDataVector.h"

namespace truth {

  bool passed_truth_jet_matching(const xAOD::Jet& jet,
                                 const xAOD::JetContainer& truthjets)                                                   
  {
    int matchedPt = 0;
    float dRmatch = 100;
    for (const auto* tjet : truthjets) {
  
      if (tjet->pt() < 10e3) continue;
    
      float dr = jet.p4().DeltaR(tjet->p4());
      if (dr < 0.3 && dr < dRmatch) {
        dRmatch = dr;
        matchedPt = tjet->pt();
      }
    }
    return (dRmatch < 0.3 && matchedPt > 20e3);
  }

  bool isWeaklyDecayingHadron(const xAOD::TruthParticle& truth_particle, int flavour)
  {
    if (isHadron(truth_particle, flavour)) {
      if ( not truth_particle.hasDecayVtx() ) { return false; }
      const xAOD::TruthVertex* vx = truth_particle.decayVtx();
      for ( size_t i = 0; i < vx->nOutgoingParticles(); i++ ) {
        const xAOD::TruthParticle* out_part = vx->outgoingParticle(i);
      if ( isHadron(*out_part, flavour) ) { return  false; }
      } 
      return true;
    }
    return false;
  }

  const xAOD::TruthParticle* getParentHadron(const xAOD::TruthParticle* truth_particle) {
    
    if ( truth_particle == nullptr ) { return nullptr; }
    if ( truth_particle->isBottomHadron() or truth_particle->isCharmHadron() ) {
      return truth_particle;
    }
    for(unsigned int p = 0; p < truth_particle->nParents(); p++) {
      const xAOD::TruthParticle* parent = truth_particle->parent(p);
      auto parent_hadron = getParentHadron(parent);
      if ( parent_hadron != nullptr ) {
        return parent_hadron;
      }
    }
    return nullptr;
  }

  bool isHadron(const xAOD::TruthParticle& truth_particle, int flavour)
  {
    if( flavour == 5 and truth_particle.isBottomHadron() ) { return true; }
    if( flavour == 4 and truth_particle.isCharmHadron()  ) { return true; }
    return false;
  }

  bool isFinalStateChargedLepton(const xAOD::TruthParticle& truth_particle) 
  {
    if ( not truth_particle.isLepton() ) { return false; }
    if ( truth_particle.charge() == 0 )  { return false; }
    if ( truth_particle.isTau() and truth_particle.status() == 2 ) { return true; }
    if ( truth_particle.status() != 1  ) { return false; }
    
    return true;
  }
  
  std::vector<int> getDRSortedIndices(std::vector<const xAOD::TruthParticle*> ConeAssocHad, const xAOD::Jet &jet) 
  {
    std::vector<float> dRofhadrons;

    for(unsigned int ip = 0; ip < ConeAssocHad.size(); ip++){
      float dr = jet.p4().DeltaR(ConeAssocHad.at(ip)->p4());
      dRofhadrons.push_back(dr);
    }

    std::vector<int> y(dRofhadrons.size());
    std::size_t n(0);
    std::generate(std::begin(y), std::end(y), [&]{ return n++; });
    std::sort(std::begin(y),std::end(y),[&](int i1, int i2) { return dRofhadrons[i1] < dRofhadrons[i2]; });

    return y;
  } 

  void getAllChildren(const xAOD::TruthParticle* particle, std::vector<const xAOD::TruthParticle*> &truthFromPart) 
  {

    if(!particle->hasDecayVtx()) { return; }

    const xAOD::TruthVertex* decayvtx = particle->decayVtx();
 
    for(unsigned i=0; i<decayvtx->nOutgoingParticles(); i++){
      const xAOD::TruthParticle* child = decayvtx->outgoingParticle(i);
      if (!child) { continue; }
      if (child->barcode() > 200e3) { continue; }
      if ((child->status()==1 || child->status()==2) && !child->isCharmHadron() && !child->isBottomHadron()) {
        truthFromPart.push_back(child);
      }
      getAllChildren(child, truthFromPart);
    }
    return;
  }

  
}
