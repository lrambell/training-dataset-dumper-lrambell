// CP interface includes
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicVariation.h"
// external tools include(s):
#include "AsgTools/AnaToolHandle.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
// #include "ParticleJetTools/JetTruthLabelingTool.h"
#include "xAODCore/ShallowCopy.h"

class JetSystematicsAlg :  public AthAlgorithm { 
public:
  JetSystematicsAlg(const std::string& name,ISvcLocator *pSvcLocator);
 
  virtual ~JetSystematicsAlg();
  
  /** Main routines specific to an ATHENA algorithm */
  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();
  

private:
  /// @brief set to true if systematics asked for and exist

  CP::SystematicSet m_ActiveSysts;
  std::string m_jet_collection;
  std::vector< std::string > m_SystNames;
  // tools
  ToolHandle<IJetCalibrationTool>        m_jetCalibrationTool_handle;
  ToolHandle<ICPJetUncertaintiesTool>    m_jetUncertaintiesTool_handle;
  float m_Sigma = 1.0;
  SG::WriteHandleKey<xAOD::JetContainer>  m_outContainerKey;
  SG::WriteHandleKey<xAOD::JetAuxContainer> m_outAuxContainerKey;
 
  StatusCode executeSystematic(const CP::SystematicSet& thisSyst, xAOD::JetContainer& inJets);
};
// class JetSystematicsAlg
