/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 */

/*
 
A tool to run jet systematic tool before retagging. 
Authorr; Bingxuan Liu (bingxuan.liu@cern.ch)

Basic structures and functions are inspired by xAODAnaHelper and AnalysisTop.

*/

#include "JetSystematicsAlg.h"

#include <map>
#include <string>

JetSystematicsAlg::JetSystematicsAlg(const std::string& name, ISvcLocator *pSvcLocator): 
  AthAlgorithm(name, pSvcLocator),   
  m_jetCalibrationTool_handle("JetCalibrationTool"),
  m_jetUncertaintiesTool_handle("JetUncertaintiesTool"),
  // mettere inizializzazione m_jet_collection 
  m_Sigma(1.0)
{
  declareProperty( "jet_collection", m_jet_collection);  
  //m_outAuxContainerKey = "AntiKt4EMPFlowJetsSysAux.";
  //m_outContainerKey = "AntiKt4EMPFlowJetsSys";
    
  declareProperty( "systematic_variations", m_SystNames);
  declareProperty( "jet_calib_tool", m_jetCalibrationTool_handle);
  declareProperty( "jet_uncert_tool", m_jetUncertaintiesTool_handle);
  declareProperty( "sigma", m_Sigma);
}

JetSystematicsAlg::~JetSystematicsAlg() = default;

StatusCode JetSystematicsAlg::initialize() {
 
  ANA_MSG_INFO( "Initializing JetSystematicAlg Interface... ");    
  m_outAuxContainerKey = m_jet_collection + "SysAux.";
  m_outContainerKey = m_jet_collection + "Sys";
  std::cout<<"TEST---------> "<<m_outAuxContainerKey<<std::endl;

  ANA_CHECK( m_outContainerKey.initialize() );
  ANA_CHECK( m_outAuxContainerKey.initialize() );
  ANA_CHECK( m_jetCalibrationTool_handle.retrieve() );
  ANA_CHECK( m_jetUncertaintiesTool_handle.retrieve() );
  std::cout<<"INIT JET COLL---------> "<<m_jet_collection<<std::endl;
  std::cout<<"INIT TEST---------> "<<m_outAuxContainerKey<<std::endl;


  //
  // Get a list of recommended systematics for this tool
  //
  ANA_MSG_INFO(" Initializing Jet Systematics :");
  for (const auto& name : m_SystNames) {

    ATH_MSG_INFO("Adding systematic variation "<< name);
    // Bing: Need to fix this as the up/down variations need to be setup separately
    m_ActiveSysts.insert(CP::SystematicVariation(name, m_Sigma));
  }

  if ( m_jetUncertaintiesTool_handle->applySystematicVariation(m_ActiveSysts) != StatusCode::SUCCESS ) {
    ANA_MSG_ERROR( "Cannot configure JetUncertaintiesTool for systematic " << m_SystNames);
    return StatusCode::FAILURE;
  }

  ANA_MSG_INFO( "JetCalibrator Interface succesfully initialized!" );

  return StatusCode::SUCCESS;

ANA_MSG_INFO(" Using Sigma Value: " << m_Sigma);
}

StatusCode JetSystematicsAlg :: execute ()
{ 
  const EventContext& ctx = Gaudi::Hive::currentContext();
  const xAOD::JetContainer* jets(nullptr);
  //ATH_MSG_INFO("Searching for " << m_jet_collection);
  ATH_CHECK( evtStore()->retrieve( jets, m_jet_collection));
  //ATH_MSG_INFO("Working on jet collection " << m_jet_collection);
  
  //ATH_MSG_INFO(" m_outContainerKey: " << m_outContainerKey);
  //ATH_MSG_INFO(" m_outAuxContainerKey: " << m_outAuxContainerKey);

  
  // Perform nominal calibration
  std::unique_ptr<xAOD::JetContainer> goodJets = std::make_unique<xAOD::JetContainer>();
  std::unique_ptr<xAOD::JetAuxContainer> goodJetsAux = std::make_unique<xAOD::JetAuxContainer>();  
  goodJets->setStore (goodJetsAux.get());  

  for(const xAOD::Jet* jet : *jets) *goodJets->emplace_back(new xAOD::Jet()) = *jet;

  for ( auto jet_itr : *goodJets ) {

    // truth labelling for systematics
    // b-jet truth labelling
    int this_TruthLabel = 0;
    bool isBjet = false; // decide whether or not the jet is a b-jet (truth-labelling + kinematic selections)
    // parton labeling scheme is used as this is the one used to derive the calibration for bjets. See: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21Summer2018SmallR#JES_Precision_Reduced_Flavour_Un
    static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID ("PartonTruthLabelID");

    if (PartonTruthLabelID.isAvailable( *jet_itr) ) {
      this_TruthLabel = PartonTruthLabelID( *jet_itr );
      if (this_TruthLabel == 5) isBjet = true;
    }
    else {
      ANA_MSG_ERROR( "PartonTruthLabelID aux data is not available!" );
      return StatusCode::FAILURE;
    }

    static SG::AuxElement::Decorator<char> accIsBjet("IsBjet"); // char due to limitations of ROOT I/O, still treat it as a bool
    accIsBjet(*jet_itr) = isBjet;
  }//for jets

  // Apply the calibration
  if ( m_jetCalibrationTool_handle->applyCalibration( *goodJets) == StatusCode::FAILURE ) {
    ANA_MSG_ERROR( "JetCalibration tool reported a StatusCode::FAILURE");
    ANA_MSG_ERROR( "TDD: JetSystematicAlg" );
    return StatusCode::FAILURE;
  }
  
  if (executeSystematic(m_ActiveSysts, *goodJets) != StatusCode::SUCCESS) {
    ANA_MSG_ERROR( "Systematic evaluation reported a StatusCode::FAILURE");
    return StatusCode::FAILURE;
  }
  
  // add ConstDataVector to TStore
  SG::WriteHandle<xAOD::JetAuxContainer> h_aux_write(m_outAuxContainerKey, ctx);
  ATH_CHECK(h_aux_write.record(std::move(goodJetsAux)));	
  SG::WriteHandle<xAOD::JetContainer>  h_write(m_outContainerKey, ctx);
  ATH_CHECK(h_write.record(std::move(goodJets)));	
  
  return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlg::executeSystematic(const CP::SystematicSet& thisSyst, xAOD::JetContainer& inJets){

  ToolHandle<ICPJetUncertaintiesTool>* jetUncTool(nullptr);

  jetUncTool = &m_jetUncertaintiesTool_handle;

  //Apply Uncertainties
  ANA_MSG_DEBUG("Configure for systematic variation : " << thisSyst.name());
  if ( (*jetUncTool)->applySystematicVariation(thisSyst) != StatusCode::SUCCESS ) {
    ANA_MSG_ERROR( "Cannot configure JetUncertaintiesTool for systematic " << thisSyst.name());
    return StatusCode::FAILURE;
  }

  for ( auto jet_itr : inJets ) {
    if ( (*jetUncTool)->applyCorrection( *jet_itr ) == CP::CorrectionCode::Error ) {
      ANA_MSG_ERROR( "JetUncertaintiesTool reported a CP::CorrectionCode::Error");
      ANA_MSG_ERROR( "TDD: JetSystematcisAlg");
    }
  }


  return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlg::finalize(){
  ANA_CHECK( m_jetCalibrationTool_handle.release() );
  ANA_CHECK( m_jetUncertaintiesTool_handle.release() );
  return StatusCode::SUCCESS;
}
