/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "JetMatcherAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include <memory>


JetMatcherAlg::JetMatcherAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
  declareProperty("floatsToCopy", m_floats.toCopy);
  declareProperty("intsToCopy", m_ints.toCopy);
  declareProperty("iparticlesToCopy", m_iparticles.toCopy);
}

StatusCode JetMatcherAlg::initialize() {
  std::string source = m_sourceJet.key();
  std::string target = m_targetJet.key();
  ATH_CHECK(m_floats.initialize(this, source, target));
  ATH_CHECK(m_ints.initialize(this, source, target));
  ATH_CHECK(m_iparticles.initialize(this, source, target));
  m_drDecorator = target + "." + m_dRKey;
  m_dPtDecorator = target + "." + m_dPtKey;
  ATH_CHECK(m_targetJet.initialize());
  ATH_CHECK(m_sourceJet.initialize());
  ATH_CHECK(m_drDecorator.initialize());
  ATH_CHECK(m_dPtDecorator.initialize());
  return StatusCode::SUCCESS;
}

namespace {
  using JL = ElementLink<xAOD::JetContainer>;
  using JC = xAOD::JetContainer;
  std::vector<const xAOD::Jet*> getJetVector(
    SG::ReadHandle<JC>& handle) {
    std::vector<const xAOD::Jet*> jets(handle->begin(), handle->end());
    std::sort(jets.begin(),jets.end(),[](auto* j1, auto* j2){
      return j1->pt() > j2->pt();
    });
    return jets;
  }
}

StatusCode JetMatcherAlg::execute(const EventContext& cxt) const {
  SG::ReadHandle<JC> targetJetGet(m_targetJet, cxt);
  SG::ReadHandle<JC> sourceJetGet(m_sourceJet, cxt);
  SG::WriteDecorHandle<JC,float> drDecorator(m_drDecorator, cxt);
  SG::WriteDecorHandle<JC,float> dPtDecorator(m_dPtDecorator, cxt);
  auto targetJets = getJetVector(targetJetGet);
  auto sourceJets = getJetVector(sourceJetGet);
  std::vector<MatchedPair<JC>> matches;
  for (const xAOD::Jet* target: targetJets) {
    auto dr = [target](const xAOD::Jet& j) {
      return target->p4().DeltaR(j.p4());
    };
    auto sourceItr = std::min_element(
      sourceJets.begin(), sourceJets.end(),
      [&dr] (auto* j1, auto* j2) {
        return dr(*j1) < dr(*j2);
      });
    if (sourceItr == sourceJets.end()) {
      drDecorator(*target) = NAN;
      dPtDecorator(*target) = NAN;
      matches.push_back({nullptr, target});
    } else {
      const xAOD::Jet* source = *sourceItr;
      drDecorator(*target) = dr(*source);
      dPtDecorator(*target) = target->pt() - source->pt();
      matches.push_back({source, target});
    }
  }
  m_floats.copy(matches, cxt);
  m_ints.copy(matches, cxt);
  m_iparticles.copy(matches, cxt);

  return StatusCode::SUCCESS;
}
StatusCode JetMatcherAlg::finalize () {
  return StatusCode::SUCCESS;
}
