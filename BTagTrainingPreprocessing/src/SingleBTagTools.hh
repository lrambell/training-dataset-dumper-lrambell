#ifndef SINGLE_BTAG_TOOLS_HH
#define SINGLE_BTAG_TOOLS_HH

#include <optional>

#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "SubjetWriter.hh"
#include "TrackSelector.hh"
#include "trackSort.hh"
#include "TruthWriter.hh"
#include "DecoratorExample.hh"
#include "JetTruthAssociator.hh"
#include "TrackTruthDecorator.hh"
#include "TrackVertexDecorator.hh"
#include "JobMetadata.hh"
#include "HitDecorator.hh"
#include "BJetShallowCopier.hh"
#include "HitWriter.hh"
#include "JetTruthAssociator.hh"
#include "JetTruthMerger.hh"
#include "TruthSelectorConfig.hh"
#include "JetLeptonDecayLabelDecorator.hh"
#include "LeptonTruthDecorator.hh"
#include "JetTruthSummaryDecorator.hh"

#include "TrackSubjetsDecorator.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"
#include "FlavorTagDiscriminants/TrackClassifier.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "xAODEgamma/ElectronContainerFwd.h"

#include "SingleBTagConfig.hh"


struct SingleBTagConfig;
struct SubjetConfig;
struct TrackConfig;
struct TruthConfig;
struct SoftElectronConfig;

class SoftElectronSelector;

class IJetLinkWriter;
class JetVertexTaggerTool;
namespace FlavorTagDiscriminants {
  class BTagMuonAugmenter;
}
class TrackLeptonDecorator;

namespace H5 {
  class H5File;
}

struct TrackToolkit {
  using AE = SG::AuxElement;
  TrackToolkit(const TrackConfig&, H5::Group&);
  TrackToolkit(TrackToolkit&&);
  TrackSelector selector;
  TrackSort sorted;
  BTagTrackWriter writer;
  SG::AuxElement::Decorator<int> n_tracks_decorator;
};


struct SoftElectronToolkit {
  using AE = SG::AuxElement;
  SoftElectronToolkit(const SoftElectronConfig&, H5::Group&);
  SoftElectronToolkit(SoftElectronToolkit&&);
  void select_and_write(const xAOD::Jet* uncalib_jet,
             const xAOD::ElectronContainer* electrons);
  private:
    std::unique_ptr<SoftElectronSelector> selector;
    std::unique_ptr<IJetLinkWriter> writer;
    AE::Decorator<int> n_electrons_decorator;
    AE::Decorator<std::vector<ElementLink<xAOD::IParticleContainer>>> m_dec_electrons_links;
};

struct SubjetToolkit {
  using AE = SG::AuxElement;

  SubjetConfig cfg;
  typedef std::vector<ElementLink<xAOD::IParticleContainer>> ParticleLinks;
  typedef ElementLink<xAOD::JetContainer> JetLink;
  AE::ConstAccessor<ParticleLinks> subjet_acc;
  AE::ConstAccessor<JetLink> parent_acc;
  SubjetToolkit(const SubjetConfig &, H5::Group&);
  SubjetWriter jet_writer;
  TrackSubjetsDecorator trkSubjetsDecorator;
  SG::AuxElement::Decorator<int> n_subjets;
  std::vector<const xAOD::Jet*> getSubjets(const xAOD::Jet* jet);
};

struct SingleBTagTools {

  using AE = SG::AuxElement;

  struct Accessors {
    Accessors(const SingleBTagConfig& cfg);
    AE::ConstAccessor<char> eventClean_looseBad;
    AE::ConstAccessor<float> jvt;
    std::function<const xAOD::BTagging*(const xAOD::Jet&)> btaggingLink;
  };

  struct Decorators {
    AE::Decorator<float> jvt;
    AE::Decorator<float> fcal_et_tev;
    AE::Decorator<int> jet_rank;
    AE::Decorator<float> trackjet_rel_dR;  // PG: temporary fix for VR track jet decorators missing
    AE::Decorator<float> trackjet_abs_dR;  // PG: temporary fix for VR track jet decorators missing
    AE::Decorator<int> n_primary_vertices;
    AE::Decorator<int> n_pileup_events;
    AE::Decorator<float> primary_vertex_detector_z;
    AE::Decorator<float> primary_vertex_beamspot_z;
    AE::Decorator<float> primary_vertex_detector_z_uncertainty;
    AE::Decorator<char> isSCT;
    Decorators():
      jvt("bTagJVT"),
      fcal_et_tev("FCal_Et_TeV"),
      jet_rank("jetPtRank"),
      trackjet_rel_dR("relativeDeltaRToVRJet"), // PG: temporary fix for VR track jet decorators missing
      trackjet_abs_dR("deltaRToVRJet"),         // PG: temporary fix for VR track jet decorators missing
      n_primary_vertices("nPrimaryVertices"),
      n_pileup_events("nTruthInteractionsPerCrossing"),
      primary_vertex_detector_z("primaryVertexDetectorZ"),
      primary_vertex_beamspot_z("primaryVertexBeamspotZ"),
      primary_vertex_detector_z_uncertainty(
        "primaryVertexDetectorZUncertainty"),
      isSCT("isSCT")
      {}
  };

  SingleBTagTools(const SingleBTagConfig&);
  ~SingleBTagTools();
  JetCalibrationTool calibration_tool;
  JetCleaningTool cleaning_tool;
  std::unique_ptr<JetVertexTaggerTool> jvttool;

  BJetShallowCopier shallow_copier;
  BTagJetAugmenter jet_augmenter;
  std::unique_ptr<FlavorTagDiscriminants::BTagMuonAugmenter> muon_augmenter;
  FlavorTagDiscriminants::TrackClassifier track_classifier;

  std::vector<std::function<void(const xAOD::BTagging&)>> dl2s;
  std::vector<std::function<void(const xAOD::Jet&)>> jet_nns;
  std::vector<std::shared_ptr<TimingStats>> timings;

  Accessors acc;
  Decorators dec;

  // truth tools
  std::vector<JetTruthAssociator> jet_truth_associators;
  std::vector<JetTruthMerger> jet_truth_mergers;
  std::vector<JetTruthSummaryDecorator> jetTruthSummaryDecorators;
  std::vector<std::function<bool(const xAOD::Jet&)>> overlap_checks;

  // jet decorators
  DecoratorExample example_decorator;
  JetLeptonDecayLabelDecorator jet_lepton_decay_label_decorator;
  LeptonTruthDecorator lepTruthDecorator;

  // track decorators
  TrackTruthDecorator trkTruthDecorator;
  TrackVertexDecorator trkVertexDecorator;
  std::unique_ptr<TrackLeptonDecorator> trkLeptonDecorator;

  // hits decorators
  std::unique_ptr<HitDecorator> hit_decorator;
};

struct SingleBTagOutputs {
  SingleBTagOutputs(const SingleBTagConfig&, H5::Group&);
  ~SingleBTagOutputs();

  BTagJetWriter jet_writer;
  std::vector<SubjetToolkit> subjets;
  std::vector<TrackToolkit> tracks;
  std::vector<TruthWriter> truths;
  std::vector<HitWriter> hits_pv;
  std::vector<HitWriter> hits_bs;
  std::unique_ptr<IJetLinkWriter> flow;
  std::unique_ptr<SoftElectronToolkit> electrons;
};

#endif
